package com.example.a2023_03_05_pradeepgedara_nycschools.data

import com.example.a2023_03_05_pradeepgedara_nycschools.data.localdb.SchoolDao
import com.example.a2023_03_05_pradeepgedara_nycschools.data.localdb.SchoolListEntity
import com.example.a2023_03_05_pradeepgedara_nycschools.data.network.DataOrException
import com.example.a2023_03_05_pradeepgedara_nycschools.data.network.SchoolApi
import com.example.a2023_03_05_pradeepgedara_nycschools.model.SchoolInformationDtoItem
import com.example.a2023_03_05_pradeepgedara_nycschools.model.SchoolListDtoItem
import javax.inject.Inject

class SchoolRepository @Inject constructor(
    private val api: SchoolApi,
    private val dao: SchoolDao
) {
    private val dataOrException = DataOrException<List<SchoolListDtoItem>, Boolean, Exception>()
    suspend fun getAllSchools(): DataOrException<List<SchoolListDtoItem>, Boolean, Exception> {
        try {
            val localData = dao.getSchoolList()
            if (localData.isNotEmpty()) {
                dataOrException.data = localData.map { it.toSchool() }
            }
            dataOrException.loading = true
            dataOrException.data = api.getListOfSchools()
            if (!dataOrException.data.isNullOrEmpty()) {
                dataOrException.loading = false
                dao.insertSchoolList(dataOrException.data!!.map { SchoolListEntity.fromDto(it) })
            }
        } catch (ex: Exception) {
            dataOrException.e = ex
        }
        return dataOrException
    }

    suspend fun getSchoolInformation(): DataOrException<List<SchoolInformationDtoItem>, Boolean, Exception> {
        val dataOrException = DataOrException<List<SchoolInformationDtoItem>, Boolean, Exception>()
        try {
            dataOrException.loading = true
            dataOrException.data = api.getSchoolInformation()
            if (dataOrException.data.toString().isNotEmpty()) {
                dataOrException.loading = false
            }
        } catch (ex: Exception) {
            dataOrException.e = ex
        }
        return dataOrException
    }

}