package com.example.a2023_03_05_pradeepgedara_nycschools.data.network

import com.example.a2023_03_05_pradeepgedara_nycschools.model.SchoolInformationDtoItem
import com.example.a2023_03_05_pradeepgedara_nycschools.model.SchoolListDtoItem
import retrofit2.http.GET
import retrofit2.http.Query
import javax.inject.Singleton

@Singleton
interface SchoolApi {
    @GET("resource/s3k6-pzi2.json")
    suspend fun getListOfSchools(): List<SchoolListDtoItem>

    @GET("resource/f9bf-2cp4.json")
    suspend fun getSchoolInformation(): List<SchoolInformationDtoItem>

}