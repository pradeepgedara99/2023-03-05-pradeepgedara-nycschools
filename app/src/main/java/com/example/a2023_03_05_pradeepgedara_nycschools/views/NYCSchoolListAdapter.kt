package com.example.a2023_03_05_pradeepgedara_nycschools.views


import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.navigation.Navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.a2023_03_05_pradeepgedara_nycschools.R
import com.example.a2023_03_05_pradeepgedara_nycschools.databinding.ItemSchoolBinding
import com.example.a2023_03_05_pradeepgedara_nycschools.model.SchoolListDtoItem


class NYCSchoolListAdapter(dataModelList: List<SchoolListDtoItem>, ctx: Context,
  private val onClick :((dtoItem: SchoolListDtoItem) -> Unit)? = null):
    RecyclerView.Adapter<NYCSchoolListAdapter.ViewHolder>(){
    private var dataModelList: List<SchoolListDtoItem>
    private val context: Context

    init {
        this.dataModelList = dataModelList
        context = ctx
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = DataBindingUtil.inflate<ItemSchoolBinding>(inflater,
            R.layout.item_school, parent,false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val dataModel: SchoolListDtoItem = dataModelList[position]
        bind(holder, dataModel)
        val schoolDetailsFragment = NYCSchoolDetailsFragment()
        val bundle = Bundle()
        bundle.putString("dbn", dataModelList[position].dbn)

    }

    private fun bind(holder: ViewHolder, dataModel: SchoolListDtoItem) {
           holder.itemRowBinding.apply {
            textViewSchoolName.text = dataModel.schoolName
            textViewSchoolAddress.text = dataModel.primaryAddress + dataModel.city + dataModel.zip
            textViewEmail.text = dataModel.schoolEmail
            textViewPhoneNumber.text = dataModel.phoneNumber

               schoolCardId.setOnClickListener {
                   onClick?.let { it1 -> it1(dataModel) }
               }
        }

    }

    override fun getItemCount(): Int {
        return dataModelList.size
    }

      class ViewHolder(val itemRowBinding: ItemSchoolBinding) :
        RecyclerView.ViewHolder(itemRowBinding.root) {


    }

    @SuppressLint("NotifyDataSetChanged")
    fun updateList(newList: List<SchoolListDtoItem>) {
        dataModelList = newList
        notifyDataSetChanged()
    }



}