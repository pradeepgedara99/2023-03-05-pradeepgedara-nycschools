package com.example.a2023_03_05_pradeepgedara_nycschools.views

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.a2023_03_05_pradeepgedara_nycschools.R

import android.util.Log
import androidx.activity.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.NavHostFragment
import com.example.a2023_03_05_pradeepgedara_nycschools.viewmodels.SchoolViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {
    private val schoolViewModel : SchoolViewModel by viewModels()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val navHostFragment =
            supportFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHostFragment
        val navController = navHostFragment.navController


    }

}