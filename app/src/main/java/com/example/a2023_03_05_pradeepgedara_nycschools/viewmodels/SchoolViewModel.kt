package com.example.a2023_03_05_pradeepgedara_nycschools.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.a2023_03_05_pradeepgedara_nycschools.data.network.DataOrException
import com.example.a2023_03_05_pradeepgedara_nycschools.data.SchoolRepository
import com.example.a2023_03_05_pradeepgedara_nycschools.model.SchoolInformationDtoItem
import com.example.a2023_03_05_pradeepgedara_nycschools.model.SchoolListDtoItem
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SchoolViewModel @Inject constructor(
    private val repository: SchoolRepository
) : ViewModel() {

    private val _schoolListLiveData =
        MutableLiveData<DataOrException<List<SchoolListDtoItem>, Boolean, Exception>>()
    val schoolListLiveData: LiveData<DataOrException<List<SchoolListDtoItem>, Boolean, Exception>>
        get() = _schoolListLiveData

    private val _schoolInfoLiveData =
        MutableLiveData<DataOrException<List<SchoolInformationDtoItem>, Boolean, Exception>>()
    val schoolInfoLiveData: LiveData<DataOrException<List<SchoolInformationDtoItem>, Boolean, Exception>>
        get() = _schoolInfoLiveData

     private val basicSchoolDetails = MutableLiveData<MutableList<String>>()


    init {
        getAllSchools()
    }

    private fun getAllSchools() {
        viewModelScope.launch {
            val result = repository.getAllSchools()
            _schoolListLiveData.postValue(result)
        }
    }

    fun getSchoolInformation() {
        viewModelScope.launch {
            val result = repository.getSchoolInformation()
            _schoolInfoLiveData.postValue(result)
        }
    }

}

