package com.example.a2023_03_05_pradeepgedara_nycschools

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class NYCSchoolApplication : Application(){

}